package com.company;

public class Main {

    public Main() {
        System.out.println("Dzia�a konstruktor klasy Main");
    }

    private Long getFactorial(final long digit) {

        long wyn = 1;
        if (digit < 0){
            return null;
        } else {
            for (int i = 2; i <= digit; i++) {
                wyn *= i;
            }
            return wyn;
        }
    }

    public static void main(String[] args) {
	    long digit = 4;
        // write your code here
        Main main = new Main();
        long factorial = main.getFactorial(digit);
        System.out.printf("Silnia(%d) = %d \n", digit, factorial);
    }
}
